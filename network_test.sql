-- ----------------------------
-- 创建数据库
-- ----------------------------
DROP DATABASE IF EXISTS `network_test`;
CREATE DATABASE `network_test` CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin';

-- ----------------------------
-- 使用数据库
-- ----------------------------
USE network_test;

-- ----------------------------
-- 创建表
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` int(0) NOT NULL,
  `title` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- 插入数据
-- ----------------------------
INSERT INTO `news` VALUES (1, '标题1', '内容1');
INSERT INTO `news` VALUES (2, '标题2', '内容2');
