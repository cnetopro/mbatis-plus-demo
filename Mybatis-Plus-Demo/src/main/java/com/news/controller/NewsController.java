package com.news.controller;

import com.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : zhuguoxu
 * @date  : 2021/1/6 1714
 */
@RestController
public class NewsController {


	@Autowired
	private NewsService service;

	@RequestMapping("news")
	public String test(){
		return service.getNews().toString();
	}
}
