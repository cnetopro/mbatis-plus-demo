package com.news.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.news.dao.NewsMapper;
import com.news.entity.News;
import com.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : zhuguoxu
 * @date  : 2021/1/6 1714
 */

@Service
public class NewsServiceImpl extends ServiceImpl<NewsMapper, News> implements NewsService {

	@Autowired
	private NewsMapper mapper;

	@DS("master")
	@Override
	public List<News> getNews() {
		return mapper.selectList(null);
	}
}
