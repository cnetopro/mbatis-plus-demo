package com.news.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.news.entity.News;

import java.util.List;

/**
 * @author : zhuguoxu
 * @date  : 2021/1/6 1714
 */
public interface NewsService extends IService<News> {
	List<News> getNews();
}
