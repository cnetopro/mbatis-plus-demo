package com.news.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.news.entity.News;
import org.apache.ibatis.annotations.Mapper;
/**
 * @author : zhuguoxu
 * @date  : 2021/1/6 1714
 */
@Mapper
public interface NewsMapper extends BaseMapper<News> {

}
