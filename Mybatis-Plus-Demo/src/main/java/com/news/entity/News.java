package com.news.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : zhuguoxu
 * @date  : 2021/1/6 1714
 */
@Data
@NoArgsConstructor
@TableName("news")
public class News {
	private int id;
	private String title;
	private String content;
}
